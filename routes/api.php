<?php

Route::get('/barangs', 'BarangController@index');
Route::post('/barang/create', 'BarangController@store');
Route::post('/barang/belanja', 'BarangController@belanja');
Route::post('/barang/delete/{id}', 'BarangController@destroy');
