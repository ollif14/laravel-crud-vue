<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\BarangResource;
use Illuminate\Http\Request;
use App\Models\Barang;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $barangs = Barang::orderBy('created_at', 'desc')-> get();

        return BarangResource::collection($barangs);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $barang = Barang::create([
            'nama' => $request->nama,
            'keterangan' => $request->keterangan,
            'stok' => $request->stok,
            'harga' => $request->harga,
        ]);

        return new BarangResource($barang);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function belanja($id){
        $barang = Barang::find($id);
        if('plus'){
            $jmlbarang =+ 1 ;
            if($jmlbarang > $barang->stok){
                return response()->json('Jumlah Baranag Tidak Cukup, Stok Yang Tersedia = ' + $barang->stok);
            }
            $sisastok = $barang->stok - $jmlbarang;
        }else if('minus') {
            $jmlbarang =- 1 ;
            if($jmlbarang > 0){
                return response()->json('Barang Tidak Bisa Minus');
            }
            $sisastok = $barang->stok + 1;
        }

        $barang->update([
            'stok' => $sisastok,
        ]);

        return new BarangResource($barang);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Barang::destroy($id);
        // Alihkan ke halaman barangs
        return "sucess";
    }
}
