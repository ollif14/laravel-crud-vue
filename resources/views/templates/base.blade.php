<!doctype html>
<html lang="en">
<head>
   <!-- Required meta tags -->
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Toko Barang</title>
</head>
<body>
   @yield('container')
   <!-- Optional JavaScript -->
   <!-- jQuery first, then Popper.js, then Bootstrap JS -->
   <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
   </script>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
   <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
   <script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>

   <script>
      new Vue({
         el:"#app",
         data :{
            nama : "",
            keterangan : "",
            stok : "",
            harga : "",
            barangs : []
         },
         methods : {
            addBarang : function(){
               let nama= this.nama;
               let keterangan= this.keterangan;
               let stok= this.stok;
               let harga= this.harga;
               this.$http.post('/api/barang/create', {nama: nama, keterangan : keterangan, stok: stok, harga : harga}).then(response => {
                  window.location.href = "/barang";
               });
            },
            removeBarang : function(index, barang){
               this.$http.post('/api/barang/delete/' + barang.id).then(response => {
                  this.barangs.splice(index, 1)
               });
            },
            toggleBelanja : function(barang){

            }
         },
         mounted : function(){
               this.$http.get('api/barangs').then(response => {
                  let result = response.body.data;
                  this.barangs = result
               });
            }
      });
   </script>
   
</body>
</html>