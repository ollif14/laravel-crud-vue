@extends('templates/base') {{-- mengambil file base.blade.php --}}
@section('title','Tambah Barang')
@section('container') {{-- Mengisi di bagian content --}}
<!-- Main Section -->
<div class="container">
<div class="row">
<!-- Content -->
<div id="app" class="col-md-12 mt-3">
<h3>Form Menambah Barang</h3>
<!-- input -->
<form @submit="addBarang">
   <div class="form-group">
         <label for="name">Nama Barang</label>
         <input class="form-control" type="text" name="nama" id="nama" v-model="nama"  placeholder="Masukan Nama Barang">
      </div>
      <div class="form-group">
         <label for="name">Keterangan Barang</label>
         <input class="form-control" type="text" name="keterangan" id="keterangan" v-model="keterangan" placeholder="Keterangan Barang">
      </div>
      <div class="form-group">
         <label for="type">Stok</label>
         <input class="form-control" type="number" name="stok" id="stok" v-model="stok" placeholder="Stok Barang">
      </div>
      <div class="form-group">
         <label for="stock">Harga</label>
         <input class="form-control" type="number" name="harga" id="harga" v-model="harga" placeholder="Harga Barang">
      </div>
    <div class="form-group float-right">
        <button class="btn btn-lg btn-danger" type="reset">Reset</button>
        <button class="btn btn-lg btn-primary" type="submit">Submit</button>
    </div>
   </form>
    <!-- input -->
</div>
<!-- /.content -->
</div>
</div>
<!-- /.Main Section -->
@endsection