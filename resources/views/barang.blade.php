@extends('templates/base')
@section('title','Barang Database')
@section('container')
<div class="container">
<div class="row">
<div class="my-4 col-12">
<h1 class="float-left">Daftar Barang</h1>
<a class="btn btn-primary float-right mt-2" href="/barang/create" role="button">Tambah Barang</a>
</div>
<div id="app" class="col-12">
    <!-- tampil -->
   <table class="table table-stripped">
      <thead class="thead-primary">
         <tr>
            <th class="text-center">No</th>
            <th>Nama Barang</th>
            <th>Keterangan Barang</th>
            <th>Stok barang</th>
            <th>Harga barang</th>
            <th>Action</th>
         </tr>
      </thead>
      <tbody>
         <tr v-for="(barang, index) in barangs">  
            <td>@{{barang.id}}</td>   
            <td>@{{barang.nama}}</td>
            <td>@{{barang.keterangan}}</td>
            <td>@{{barang.stok}}</td>
            <td>@{{barang.harga}}</td>
            <td>
               <button class="btn btn-xs btn-danger" v-on:click="removeBarang(index, barang)">Delete</button>
            </td>
         </tr>
      </tbody>
   </table>
</div>
<!-- tampil -->
</div>
</div>